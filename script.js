//preciso colocar cada uma das frases da wikipedia num array. 15 frases no total
//sortear a posicao do array para mostrar a frase ao clicar
//fazer um handler de clique e passar a funcao dentro
// estilizar pra ficar bonitinho, coloca um logo no header 
const respostas = [
    //afirmativa
    "Com certeza!",
    "Sem duvidas.",
    "A meu ver, sim!",
    "SIM! - definitivamente.",
    "Pode contar com isso.",
    //sem compromisso
    "Resposta nebulosa, tente novamente.",
    "Pergunte novamente mais tarde.",
    "Melhor eu não te contar agora.",
    "Não posso prever agora.",
    "Concentre-se e pergunte novamente.",
    //negativas
    "Não conte com isso.",
    "Minha resposta é não.",
    "Minhas fontes dizem não.",
    "Perspectiva não tão boa.",
    "Muito duvidoso."


]


const layout = document.getElementById("output")
const button = document.getElementById("8magic");
// const color_box = document.getElementById("color_box");

button.onclick = function(){
    let aleatory = Math.floor((Math.random() * respostas.length));
    layout.innerText = respostas[aleatory]
    console.log(aleatory)

    if (aleatory < 5){
        layout.className = "positivas";
    }
    if (aleatory >= 5 && aleatory <= 9){
        layout.className = "neutras";
    }
    if (aleatory >= 10 ){
        layout.className = "negativas";
    }
}